package jsonapi

import "golang.org/x/net/context"

type key int

var (
	reqKey key
	resKey key = 1
)

// NewRequestContext returns a context that carries a request.
func NewRequestContext(ctx context.Context, req *Request) context.Context {
	return context.WithValue(ctx, reqKey, req)
}

// RequestFromContext returns the request stored in ctx.
func RequestFromContext(ctx context.Context) *Request {
	return ctx.Value(reqKey).(*Request)
}

// NewResponseContext returns a context that carries a response.
func NewResponseContext(ctx context.Context, res *Response) context.Context {
	return context.WithValue(ctx, resKey, res)
}

// ResponseFromContext returns the response stored in ctx.
func ResponseFromContext(ctx context.Context) *Response {
	return ctx.Value(resKey).(*Response)
}
