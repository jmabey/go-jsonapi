package jsonapi

import (
	"fmt"
	"net/http"

	"gopkg.in/guregu/null.v3"
)

// Resource error format strings.
const (
	ErrRelationshipMissing = "Missing required relationship %q."
	ErrRelationshipSingle  = "Relationship %q must be a single resource linkage."
	ErrRelationshipType    = "Relationship %q must have type %q."
)

// Fieldsets defines a map of sparse fieldsets which specifies a slice of
// allowed fields for resource types. Key is resource type, value is a slice of
// field names.
type Fieldsets map[string][]string

// Resource represents a resource object.
type Resource struct {
	ID            string        `json:"id"`
	Type          string        `json:"type"`
	Attributes    Attributes    `json:"attributes,omitempty"`
	Relationships Relationships `json:"relationships,omitempty"`
	Links         Links         `json:"links,omitempty"`
	Meta          Meta          `json:"meta,omitempty"`
}

// Resource returns the resource itself. This implements the Resourcer
// interface.
func (r *Resource) Resource() *Resource {
	return r
}

// NullLinkageID returns the ID of an optional resource linkage with
// relationship key k and an expected type of t.
func (r Resource) NullLinkageID(k, t string) (null.String, error) {
	rel, ok := r.Relationships[k]
	if !ok {
		return null.String{}, nil
	}
	if rel.Data == nil || rel.Data.Resource == nil {
		return null.String{}, Error{
			Status: http.StatusBadRequest,
			Detail: fmt.Sprintf(ErrRelationshipSingle, k),
		}
	}
	if rel.Data.Resource.Type != t {
		return null.String{}, Error{
			Status: http.StatusBadRequest,
			Detail: fmt.Sprintf(ErrRelationshipType, k, t),
		}
	}
	return null.StringFrom(rel.Data.Resource.ID), nil
}

// LinkageID returns the ID of the resource linkage with relationship key k and
// an expected type of t.
func (r Resource) LinkageID(k, t string) (string, error) {
	id, err := r.NullLinkageID(k, t)
	if err != nil {
		return "", err
	}
	if !id.Valid {
		return "", Error{
			Status: http.StatusBadRequest,
			Detail: fmt.Sprintf(ErrRelationshipMissing, k),
		}
	}
	return id.String, nil
}

// AddSingleLinkage adds a resource linkage object to r's relationships for
// a single resource. The relationship will have key k and the linked resource
// has ID id and type t.
func (r *Resource) AddSingleLinkage(k, id, t string) {
	if r.Relationships == nil {
		r.Relationships = Relationships{}
	}
	r.Relationships[k] = &Relationship{
		Data: &Data{
			Resource: &Resource{ID: id, Type: t},
		},
	}
}

// Sparse removes attributes and relationships not found in the given sparse
// fieldsets. This also modifies all nested resources found under the resource's
// relationships.
func (r *Resource) Sparse(fs Fieldsets) {
	if f := fs[r.Type]; f != nil {
		as := Attributes{}
		rs := Relationships{}
		for _, k := range f {
			if v, ok := r.Attributes[k]; ok {
				as[k] = v
			}
			if v, ok := r.Relationships[k]; ok {
				rs[k] = v
			}
		}
		r.Attributes = as
		r.Relationships = rs
	}

	for _, rel := range r.Relationships {
		if rel.Data.Resource != nil {
			rel.Data.Resource.Sparse(fs)
		}
		rel.Data.Resources.Sparse(fs)
	}
}

// Resources is a slice of resources.
type Resources []*Resource

// IDs returns a slice of IDs for each resource in rs.
func (rs Resources) IDs() []string {
	ids := []string{}
	for _, r := range rs {
		ids = append(ids, r.ID)
	}
	return ids
}

// Sparse removes attributes and relationships not found in the given sparse
// fieldsets for all resources in rs. This also modifies all nested resources
// found under each resource's relationships.
func (rs Resources) Sparse(fs Fieldsets) {
	for _, r := range rs {
		r.Sparse(fs)
	}
}

// Resources returns the resource slice itself. This implements the Resourcers
// interface.
func (rs Resources) Resources() Resources {
	return rs
}

// Resourcer is an interface that wraps a Resource method for returning
// a resource.
type Resourcer interface {
	Resource() *Resource
}

// Resourcers is an interface that wraps a Resources method for returning
// a slice of resources.
type Resourcers interface {
	Resources() Resources
}
