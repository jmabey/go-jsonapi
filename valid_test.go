package jsonapi

import (
	"net/http"
	"testing"
)

func TestValidAccept(t *testing.T) {
	for i, tt := range []struct {
		in, err string
	}{
		{"application/vnd.api+json", ""},
		{"*,application/vnd.api+json", ""},
		{"*", `Request must have "application/vnd.api+json" type in Accept header.`},
	} {
		req, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			t.Fatal("request error:", err)
		}
		req.Header.Set("Accept", tt.in)

		err = ValidAccept(req)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error %q", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got %q; want %q", i, err, tt.err)
		}
	}
}

func TestValidParams(t *testing.T) {
	for i, tt := range []struct {
		url, err string
	}{
		{"?filter=f&include=i&page[size]=12&sort=s", ""},
		{"?goodKey=1&good-key=2&good_key=3", ""},
		{"?bad=1", `Request has invalid parameter "bad".`},
	} {
		req, err := http.NewRequest("GET", tt.url, nil)
		if err != nil {
			t.Fatal("request error:", err)
		}

		err = ValidParams(req)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error %q", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got %q; want %q", i, err, tt.err)
		}
	}
}

func TestValidIncludes(t *testing.T) {
	for i, tt := range []struct {
		incs, valid Includes
		err         string
	}{
		{
			Includes{},
			nil,
			"",
		},
		{
			Includes{"key"},
			Includes{"key"},
			"",
		},
		{
			Includes{"two", "one"},
			Includes{"one", "two"},
			"",
		},
		{
			Includes{"bad"},
			nil,
			`Request has unsupported include "bad".`,
		},
		{
			Includes{"bad"},
			Includes{"good"},
			`Request has unsupported include "bad".`,
		},
	} {
		req := &Request{Includes: tt.incs}
		err := ValidIncludes(req, tt.valid)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error %q", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got %q; want %q", i, err, tt.err)
		}
	}
}

func TestValidSorts(t *testing.T) {
	for i, tt := range []struct {
		ss, valid Sorts
		err       string
	}{
		{
			Sorts{},
			nil,
			"",
		},
		{
			Sorts{{Name: "one"}},
			Sorts{{Name: "one"}},
			"",
		},
		{
			Sorts{{Name: "bad"}},
			nil,
			`Request has unsupported sort field "bad".`,
		},
		{
			Sorts{{Name: "bad"}},
			Sorts{{Name: "good"}},
			`Request has unsupported sort field "bad".`,
		},
	} {
		req := &Request{Sorts: tt.ss}
		err := ValidSorts(req, tt.valid)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error %q", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got %q; want %q", i, err, tt.err)
		}
	}
}

func TestClientID(t *testing.T) {
	for i, tt := range []struct {
		require Requirement
		method  string
		data    *Data
		err     string
	}{
		{
			Required,
			http.MethodPatch,
			&Data{Resource: &Resource{}},
			"",
		},
		{
			Required,
			http.MethodPost,
			nil,
			"",
		},
		{
			Required,
			http.MethodPost,
			&Data{},
			"",
		},
		{
			Required,
			http.MethodPost,
			&Data{Resource: &Resource{}},
			ErrMissingClientID.Detail,
		},
		{
			Required,
			http.MethodPost,
			&Data{Resources: Resources{{}}},
			ErrMissingClientID.Detail,
		},
		{
			Required,
			http.MethodPost,
			&Data{Resource: &Resource{ID: "id1"}, Resources: Resources{{ID: "id2"}}},
			"",
		},
		{
			Optional,
			http.MethodPost,
			&Data{Resources: Resources{{}, {ID: "id"}}},
			"",
		},
		{
			Forbidden,
			http.MethodPost,
			&Data{Resource: &Resource{}},
			"",
		},
		{
			Forbidden,
			http.MethodPost,
			&Data{Resource: &Resource{ID: "id"}},
			ErrClientID.Detail,
		},
	} {
		req, err := http.NewRequest(tt.method, "/", nil)
		if err != nil {
			t.Errorf("%d: unexpected request error: %s", i, err)
			continue
		}

		jreq, err := NewRequest(req)
		if err != nil {
			t.Errorf("%d: unexpected JSON API request error: %s", i, err)
			continue
		}
		jreq.Data = tt.data

		err = ValidClientIDs(req, jreq, tt.require)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error: %s", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got %q; want %q", i, err, tt.err)
		}
	}
}

func TestValidType(t *testing.T) {
	for i, tt := range []struct {
		typ  string
		data *Data
		err  string
	}{
		{
			"",
			&Data{Resource: &Resource{Type: "type"}},
			"",
		},
		{
			"type",
			nil,
			"",
		},
		{
			"type",
			&Data{Resource: &Resource{Type: "type"}, Resources: Resources{{Type: "type"}}},
			"",
		},
		{
			"type",
			&Data{Resource: &Resource{Type: "res"}},
			`Resources must have type "type".`,
		},
		{
			"type",
			&Data{Resources: Resources{{Type: "res"}}},
			`Resources must have type "type".`,
		},
	} {
		err := ValidType(&Request{Data: tt.data}, tt.typ)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error: %s", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got %q; want %q", i, err, tt.err)
		}
	}
}

func TestValidDataTypes(t *testing.T) {
	for i, tt := range []struct {
		bits   DataTypeBits
		method string
		data   *Data
		err    string
	}{
		{
			0,
			http.MethodGet,
			nil,
			"",
		},
		{
			0,
			http.MethodGet,
			&Data{Resource: &Resource{}},
			ErrDataSingle.Detail,
		},
		{
			0,
			http.MethodGet,
			&Data{Resources: Resources{}},
			ErrDataArray.Detail,
		},
		{
			0,
			http.MethodPost,
			nil,
			ErrDataNull.Detail,
		},
		{
			0,
			http.MethodPost,
			&Data{Resource: &Resource{}},
			"",
		},
		{
			0,
			http.MethodPost,
			&Data{Resources: Resources{}},
			ErrDataArray.Detail,
		},
		{
			DataArray,
			http.MethodPost,
			&Data{Resources: Resources{}},
			"",
		},
		{
			DataNull | DataSingle | DataArray,
			http.MethodPost,
			nil,
			"",
		},
		{
			DataNull | DataSingle | DataArray,
			http.MethodPost,
			&Data{Resource: &Resource{}},
			"",
		},
		{
			DataNull | DataSingle | DataArray,
			http.MethodPost,
			&Data{Resources: Resources{}},
			"",
		},
	} {
		req := &http.Request{Method: tt.method}
		jreq := &Request{Data: tt.data}

		err := ValidDataTypes(req, jreq, tt.bits)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error: %s", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got %q; want %q", i, err, tt.err)
		}
	}
}
