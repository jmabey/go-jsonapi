package jsonapi

// Wildcard matches any include path.
const Wildcard = "*"

// Includes stores a slice of resource inclusion paths.
type Includes []string

// Any represents all include paths.
var Any = Includes{Wildcard}

// Contains returns true if p is found in is or is contains the wildcard value.
func (is Includes) Contains(p string) bool {
	for _, v := range is {
		if v == Wildcard || v == p {
			return true
		}
	}
	return false
}
