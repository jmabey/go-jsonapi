package jsonapi

import (
	"testing"
	"time"

	"gopkg.in/guregu/null.v3"
)

func TestAttributesBool(t *testing.T) {
	for i, tt := range []struct {
		in      interface{}
		null    null.Bool
		nonNull bool
	}{
		{nil, null.Bool{}, false},
		{"string", null.Bool{}, false},
		{false, null.BoolFrom(false), false},
		{true, null.BoolFrom(true), true},
	} {
		a := Attributes{"value": tt.in}
		if got := a.NullBool("value"); got != tt.null {
			t.Errorf("%d: got null %v; want %v", i, got, tt.null)
		}
		if got := a.Bool("value"); got != tt.nonNull {
			t.Errorf("%d: got non-null %t; want %t", i, got, tt.nonNull)
		}
	}
}

func TestAttributesFloat(t *testing.T) {
	for i, tt := range []struct {
		in      interface{}
		null    null.Float
		nonNull float64
	}{
		{nil, null.Float{}, 0},
		{"string", null.Float{}, 0},
		{float64(1.234), null.FloatFrom(1.234), 1.234},
	} {
		a := Attributes{"value": tt.in}
		if got := a.NullFloat("value"); got != tt.null {
			t.Errorf("%d: got null %v; want %v", i, got, tt.null)
		}
		if got := a.Float("value"); got != tt.nonNull {
			t.Errorf("%d: got non-null %f; want %f", i, got, tt.nonNull)
		}
	}
}

func TestAttributesInt(t *testing.T) {
	for i, tt := range []struct {
		in      interface{}
		null    null.Int
		nonNull int
	}{
		{nil, null.Int{}, 0},
		{"string", null.Int{}, 0},
		{int(123), null.IntFrom(123), 123},
		{int64(123), null.IntFrom(123), 123},
		{float64(123), null.IntFrom(123), 123},
	} {
		a := Attributes{"value": tt.in}
		if got := a.NullInt("value"); got != tt.null {
			t.Errorf("%d: got null %v; want %v", i, got, tt.null)
		}
		if got := a.Int("value"); got != tt.nonNull {
			t.Errorf("%d: got non-null %d; want %d", i, got, tt.nonNull)
		}
	}
}

func TestAttributesString(t *testing.T) {
	for i, tt := range []struct {
		in      interface{}
		null    null.String
		nonNull string
	}{
		{nil, null.String{}, ""},
		{"string", null.StringFrom("string"), "string"},
		{123, null.String{}, ""},
	} {
		a := Attributes{"value": tt.in}
		if got := a.NullString("value"); got != tt.null {
			t.Errorf("%d: got null %v; want %v", i, got, tt.null)
		}
		if got := a.String("value"); got != tt.nonNull {
			t.Errorf("%d: got non-null %s; want %s", i, got, tt.nonNull)
		}
	}
}

func TestAttributesTime(t *testing.T) {
	d := time.Date(2016, 3, 25, 0, 0, 0, 0, time.UTC)
	for i, tt := range []struct {
		in      interface{}
		null    null.Time
		nonNull time.Time
	}{
		{nil, null.Time{}, time.Time{}},
		{"string", null.Time{}, time.Time{}},
		{123, null.Time{}, time.Time{}},
		{"2016-03-25T00:00:00Z", null.TimeFrom(d), d},
	} {
		a := Attributes{"value": tt.in}
		if got := a.NullTime("value"); got != tt.null {
			t.Errorf("%d: got null %v; want %v", i, got, tt.null)
		}
		if got := a.Time("value"); got != tt.nonNull {
			t.Errorf("%d: got non-null %s; want %s", i, got, tt.nonNull)
		}
	}
}
