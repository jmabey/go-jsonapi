package jsonapi

import (
	"fmt"
	"regexp"
	"strings"
)

// Standard parameter key constants.
const (
	ParamFields     = "fields"
	ParamFilter     = "filter"
	ParamInclude    = "include"
	ParamPage       = "page"
	ParamPageNumber = "page[number]"
	ParamPageSize   = "page[size]"
	ParamPageOffset = "page[offset]"
	ParamPageLimit  = "page[limit]"
	ParamPageCursor = "page[cursor]"
	ParamSort       = "sort"
)

// StandardParams is a map containing all standard query parameters defined by
// JSON API.
var StandardParams = map[string]bool{
	ParamFields:  true,
	ParamFilter:  true,
	ParamInclude: true,
	ParamPage:    true,
	ParamSort:    true,
}

// RegexpStandardParam matches strings following the naming convention for
// standard parameters.
var RegexpStandardParam = regexp.MustCompile(`^[a-z]+$`)

// RegexpFields matches fields parameter keys. First submatch is the resource
// type.
var RegexpFields = regexp.MustCompile(`^fields\[(.*)\]$`)

// SplitComma splits a comma-separated list as a string into a slice.
func SplitComma(s string) []string {
	if s == "" {
		return []string{}
	}
	return strings.Split(s, ",")
}

// SplitParamKey splits a parameter key into a slice. The parameter key may use
// square bracket syntax to indicate nested values. For example, "key[one][two]"
// returns the slice [key one two].
func SplitParamKey(k string) []string {
	out := []string{}
	if k == "" {
		out = append(out, k)
	}
	for k != "" {
		start := strings.SplitN(k, "[", 2)
		if len(start) < 2 {
			out = append(out, k)
			break
		}

		end := strings.SplitN(start[1], "]", 2)
		if len(end) < 2 {
			out = append(out, k)
			break
		}

		if start[0] != "" {
			out = append(out, start[0])
		}
		out = append(out, end[0])
		k = end[1]
	}
	return out
}

// JoinParamKey joins a parameter key slice into a string.
func JoinParamKey(ks []string) string {
	for i := 1; i < len(ks); i++ {
		ks[i] = fmt.Sprintf("[%s]", ks[i])
	}
	return strings.Join(ks, "")
}
