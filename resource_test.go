package jsonapi

import (
	"reflect"
	"testing"

	"gopkg.in/guregu/null.v3"
)

func TestLinkageID(t *testing.T) {
	for i, tt := range []struct {
		rels     Relationships
		outNull  null.String
		errNull  string
		out, err string
	}{
		{
			nil,
			null.String{}, "",
			"", `Missing required relationship "key".`,
		},
		{
			Relationships{"key": {}},
			null.String{}, `Relationship "key" must be a single resource linkage.`,
			"", `Relationship "key" must be a single resource linkage.`,
		},
		{
			Relationships{"key": {Data: &Data{}}},
			null.String{}, `Relationship "key" must be a single resource linkage.`,
			"", `Relationship "key" must be a single resource linkage.`,
		},
		{
			Relationships{
				"key": {
					Data: &Data{
						Resource: &Resource{ID: "id", Type: "bad"},
					},
				},
			},
			null.String{}, `Relationship "key" must have type "type".`,
			"", `Relationship "key" must have type "type".`,
		},
		{
			Relationships{
				"key": {
					Data: &Data{
						Resource: &Resource{ID: "id", Type: "type"},
					},
				},
			},
			null.StringFrom("id"), "",
			"id", "",
		},
	} {
		res := &Resource{Relationships: tt.rels}

		outNull, err := res.NullLinkageID("key", "type")
		if tt.errNull == "" && err != nil {
			t.Errorf("%d: unexpected null error: %q", i, err)
		}
		if tt.errNull != "" && (err == nil || tt.errNull != err.Error()) {
			t.Errorf("%d: got null error %q; want %q", i, err, tt.errNull)
		}
		if outNull != tt.outNull {
			t.Errorf("%d: got null %v; want %v", i, outNull, tt.outNull)
		}

		out, err := res.LinkageID("key", "type")
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error: %q", i, err)
		}
		if tt.err != "" && (err == nil || tt.err != err.Error()) {
			t.Errorf("%d: got error %q; want %q", i, err, tt.err)
		}
		if out != tt.out {
			t.Errorf("%d: got %s; want %s", i, out, tt.out)
		}
	}
}

func TestResourceAddSingleLinkage(t *testing.T) {
	want := &Resource{
		Relationships: Relationships{
			"model": &Relationship{
				Data: &Data{
					Resource: &Resource{ID: "mid", Type: "modeltype"},
				},
			},
		},
	}

	r := &Resource{}
	r.AddSingleLinkage("model", "mid", "modeltype")

	if !reflect.DeepEqual(r, want) {
		t.Errorf("got %v; want %v", r, want)
	}
}

func TestResourceSparse(t *testing.T) {
	for i, tt := range []struct {
		fs      Fieldsets
		in, out *Resource
	}{
		// No fields defined for type.
		{
			Fieldsets{},
			&Resource{
				Type:       "type",
				Attributes: Attributes{"key": "value"},
			},
			&Resource{
				Type:       "type",
				Attributes: Attributes{"key": "value"},
			},
		},

		// Fields are applied to the resource itself.
		{
			Fieldsets{"type": {"one", "two"}},
			&Resource{
				Type:       "type",
				Attributes: Attributes{"one": 1, "two": 2, "three": 3},
			},
			&Resource{
				Type:          "type",
				Attributes:    Attributes{"one": 1, "two": 2},
				Relationships: Relationships{},
			},
		},

		// Relationships can be excluded.
		{
			Fieldsets{"type": {}},
			&Resource{
				Type: "type",
				Relationships: Relationships{
					"rel": {
						Data: &Data{
							Resource: &Resource{Type: "type"},
						},
					},
				},
			},
			&Resource{
				Type:          "type",
				Attributes:    Attributes{},
				Relationships: Relationships{},
			},
		},

		// Fields are applied to related single resources.
		{
			Fieldsets{"type": {"one"}},
			&Resource{
				Type: "rels",
				Relationships: Relationships{
					"rel": {
						Data: &Data{
							Resource: &Resource{
								Type:       "type",
								Attributes: Attributes{"one": 1, "two": 2},
							},
						},
					},
				},
			},
			&Resource{
				Type: "rels",
				Relationships: Relationships{
					"rel": {
						Data: &Data{
							Resource: &Resource{
								Type:          "type",
								Attributes:    Attributes{"one": 1},
								Relationships: Relationships{},
							},
						},
					},
				},
			},
		},

		// Fields are applied to related resource slice.
		{
			Fieldsets{"type": {"one", "rels"}},
			&Resource{
				Type: "rels",
				Relationships: Relationships{
					"rels": {
						Data: &Data{
							Resources: Resources{
								{
									Type:       "type",
									Attributes: Attributes{"one": 1, "two": 2},
								},
								{
									Type:       "type",
									Attributes: Attributes{"one": 3, "two": 4},
								},
							},
						},
					},
				},
			},
			&Resource{
				Type: "rels",
				Relationships: Relationships{
					"rels": {
						Data: &Data{
							Resources: Resources{
								{
									Type:          "type",
									Attributes:    Attributes{"one": 1},
									Relationships: Relationships{},
								},
								{
									Type:          "type",
									Attributes:    Attributes{"one": 3},
									Relationships: Relationships{},
								},
							},
						},
					},
				},
			},
		},

		// Fields are applied to deeply nested resources.
		{
			Fieldsets{"type": []string{"one", "rel"}},
			&Resource{
				Type: "type",
				Relationships: Relationships{
					"rel": {
						Data: &Data{
							Resource: &Resource{
								Type: "type",
								Relationships: Relationships{
									"rel": {
										Data: &Data{
											Resource: &Resource{
												Type:       "type",
												Attributes: Attributes{"one": 1, "two": 2},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			&Resource{
				Type:       "type",
				Attributes: Attributes{},
				Relationships: Relationships{
					"rel": {
						Data: &Data{
							Resource: &Resource{
								Type:       "type",
								Attributes: Attributes{},
								Relationships: Relationships{
									"rel": {
										Data: &Data{
											Resource: &Resource{
												Type:          "type",
												Attributes:    Attributes{"one": 1},
												Relationships: Relationships{},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	} {
		tt.in.Sparse(tt.fs)

		if !reflect.DeepEqual(tt.in, tt.out) {
			t.Errorf("%d: got %v; want %v", i, tt.in, tt.out)
		}
	}
}

func TestResourcesIDs(t *testing.T) {
	rs := Resources{&Resource{ID: "a"}, &Resource{ID: "b"}}

	if got, want := rs.IDs(), []string{"a", "b"}; !reflect.DeepEqual(got, want) {
		t.Errorf("got %v; want %v", got, want)
	}
}
