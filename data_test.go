package jsonapi

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

func TestDataMarshalJSON(t *testing.T) {
	for i, tt := range []struct {
		d Data
		b []byte
	}{
		{
			Data{},
			[]byte("null"),
		},
		{
			Data{Resource: &Resource{ID: "id"}},
			[]byte(`{"id":"id","type":""}`),
		},
		{
			Data{Resources: Resources{}},
			[]byte(`[]`),
		},
		{
			Data{Resources: Resources{{ID: "1"}, {ID: "2"}}},
			[]byte(`[{"id":"1","type":""},{"id":"2","type":""}]`),
		},
	} {
		b, err := json.Marshal(tt.d)

		if err != nil {
			t.Errorf("%d: unexpected error %s", i, err)
		}
		if bytes.Compare(b, tt.b) != 0 {
			t.Errorf("%d: got %s; want %s", i, b, tt.b)
		}
	}
}

func TestDataUnmarshalJSON(t *testing.T) {
	for i, tt := range []struct {
		b []byte
		d Data
	}{
		{
			[]byte("null"),
			Data{},
		},
		{
			[]byte(`{"id":"id"}`),
			Data{Resource: &Resource{ID: "id"}},
		},
		{
			[]byte("[]"),
			Data{Resources: Resources{}},
		},
		{
			[]byte(`[{"id":"1"},{"id":"2"}]`),
			Data{Resources: Resources{{ID: "1"}, {ID: "2"}}},
		},
	} {
		var d Data
		err := json.Unmarshal(tt.b, &d)

		if err != nil {
			t.Errorf("%d: unexpected error %s", i, err)
		}
		if !reflect.DeepEqual(d, tt.d) {
			t.Errorf("%d: got %v; want %v", i, d, tt.d)
		}
	}
}
