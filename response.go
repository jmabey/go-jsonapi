package jsonapi

import (
	"encoding/json"
	"log"
	"net/http"
)

// Commonly used header names.
const (
	HeaderContentType = "Content-Type"
	HeaderLocation    = "Location"
)

// Response represents a JSON API response document.
type Response struct {
	rw http.ResponseWriter

	Status int `json:"-"`

	JSONAPI  *JSONAPI  `json:"jsonapi,omitempty"`
	Links    Links     `json:"links,omitempty"`
	Data     *Data     `json:"data,omitempty"`
	Included Resources `json:"included,omitempty"`
	Errors   []Error   `json:"errors,omitempty"`
	Meta     Meta      `json:"meta,omitempty"`

	// Fieldsets defines sparse fieldsets to be applied to all resources in the
	// response when the response is written.
	Fieldsets Fieldsets `json:"-"`
}

// NewResponse initializes a new response to be written to rw.
func NewResponse(rw http.ResponseWriter) *Response {
	return &Response{
		rw:      rw,
		JSONAPI: &JSONAPI{Version: Version},
	}
}

// Header returns the headers for the response.
func (r *Response) Header() http.Header {
	return r.rw.Header()
}

// SetLocation sets the Location header to some URL of a created resource.
func (r *Response) SetLocation(url string) {
	r.Header().Set(HeaderLocation, url)
}

// SetResource sets the response data to a single resource.
func (r *Response) SetResource(rr Resourcer) {
	r.Data = &Data{Resource: rr.Resource()}
}

// AddResource adds a single resource to the response data.
func (r *Response) AddResource(rr Resourcer) {
	if r.Data == nil {
		r.Data = &Data{}
	}
	r.Data.Resource = nil
	r.Data.Resources = append(r.Data.Resources, rr.Resource())
}

// AddResources adds all resources in rrs to the response data.
func (r *Response) AddResources(rrs Resourcers) {
	if r.Data == nil {
		r.Data = &Data{}
	}
	if r.Data.Resources == nil {
		r.Data.Resources = Resources{}
	}
	for _, res := range rrs.Resources() {
		r.AddResource(res)
	}
}

// AddInclude adds a single resource to the slice of included resources. If the
// resource is already in the slice, nothing happens.
func (r *Response) AddInclude(rr Resourcer) {
	res := rr.Resource()
	for _, ir := range r.Included {
		if ir.ID == res.ID && ir.Type == res.Type {
			return
		}
	}
	r.Included = append(r.Included, res)
}

// AddIncludes adds all resources in rrs to the slice of included resources. If
// a resource is already in the slice, it won't be added as a duplicate.
func (r *Response) AddIncludes(rrs Resourcers) {
	for _, res := range rrs.Resources() {
		r.AddInclude(res)
	}
}

// AddError adds an error to the response and updates the status code.
func (r *Response) AddError(err error) {
	jerr := Error{Detail: err.Error()}
	switch err := err.(type) {
	case Error:
		jerr = err
	case Errors:
		for _, e := range err {
			r.AddError(e)
		}
		return
	}

	r.Errors = append(r.Errors, jerr)

	switch {
	case len(r.Errors) == 1:
		r.Status = jerr.Status
	case len(r.Errors) > 1:
		// Multiple errors pick a status based on all of the errors. If all
		// errors with a status set have the same status, that status is used.
		// Otherwise, the largest, most general status (x00) is used.
		r.Status = 0
		same, set := 0, len(r.Errors)
		for _, err := range r.Errors {
			switch {
			case err.Status == 0:
				set--
			case err.Status == r.Status:
				same++
			case err.Status > r.Status:
				r.Status, same = err.Status, 1
			}
		}
		if same != set {
			r.Status = r.Status / 100 * 100
		}
	}
	if r.Status == 0 {
		r.Status = http.StatusInternalServerError
	}
}

// Write writes the entire response to the response writer. This applies sparse
// fieldsets to all resources to be written. This should only be called once.
func (r *Response) Write() {
	if r.Status == 0 {
		r.Status = http.StatusOK
	}

	r.rw.Header().Set(HeaderContentType, ContentType)
	r.rw.WriteHeader(r.Status)

	if r.Status == http.StatusNoContent {
		return
	}

	if r.Data != nil {
		if r.Data.Resource != nil {
			r.Data.Resource.Sparse(r.Fieldsets)
		}
		r.Data.Resources.Sparse(r.Fieldsets)
	}
	r.Included.Sparse(r.Fieldsets)

	if b, err := json.Marshal(r); err != nil {
		log.Println("jsonapi: marshall response:", err)
	} else if _, err := r.rw.Write(b); err != nil {
		log.Println("jsonapi: write response:", err)
	}
}
