package jsonapi

import "encoding/json"

// Data represents a resource or a collection of resources.
type Data struct {
	Resource  *Resource
	Resources Resources
}

// MarshalJSON marshals the struct to either a single resource or array of
// resources.
func (d Data) MarshalJSON() ([]byte, error) {
	if d.Resource != nil {
		return json.Marshal(d.Resource)
	}
	return json.Marshal(d.Resources)
}

// UnmarshalJSON unmarshals JSON into the struct. The JSON can be a single
// resource or an array of resource.
func (d *Data) UnmarshalJSON(b []byte) error {
	if b[0] == '[' {
		d.Resource = nil
		return json.Unmarshal(b, &d.Resources)
	}
	d.Resource = &Resource{}
	d.Resources = nil
	return json.Unmarshal(b, &d.Resource)
}
