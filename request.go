package jsonapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// ErrInvalidJSON is the error format string used when a request's JSON body is
// invalid.
const ErrInvalidJSON = "Invalid JSON body: %s"

// Page stores pagination parameter values.
type Page struct {
	Number int
	Size   int
	Offset int
	Limit  int
	Cursor string
}

// Request contains JSON API request data extracted from an http.Request.
type Request struct {
	Data *Data `json:"data"`

	// Params contains the request query parameters.
	Params url.Values `json:"-"`

	// Includes is a list of requested relationship paths. nil means the client
	// did not specify a list.
	Includes Includes `json:"-"`

	// Fieldsets is the sparse fieldsets specified in the request.
	Fieldsets Fieldsets `json:"-"`

	// Sorts contains the sort fields specified in the request.
	Sorts Sorts `json:"-"`

	// Page contains pagination parameter values.
	Page Page `json:"-"`
}

// NewRequest returns a new request populated from an http.Request.
func NewRequest(req *http.Request) (*Request, error) {
	jreq := &Request{
		Params:    req.URL.Query(),
		Fieldsets: Fieldsets{},
	}

	if req.Body != nil {
		if err := json.NewDecoder(req.Body).Decode(jreq); err != nil && err != io.EOF {
			return jreq, Error{
				Status: http.StatusBadRequest,
				Detail: fmt.Sprintf(ErrInvalidJSON, err),
			}
		}
	}

	if _, ok := jreq.Params[ParamInclude]; ok {
		jreq.Includes = SplitComma(jreq.Param(ParamInclude))
	}

	for k := range jreq.Params {
		if sm := RegexpFields.FindStringSubmatch(k); sm != nil {
			jreq.Fieldsets[sm[1]] = SplitComma(jreq.Param(k))
		}
	}

	for _, n := range SplitComma(jreq.Param(ParamSort)) {
		jreq.Sorts = append(jreq.Sorts, &Sort{
			Name: strings.TrimPrefix(n, "-"),
			Desc: strings.HasPrefix(n, "-"),
		})
	}

	jreq.Page.Number = jreq.ParamNonNegInt(ParamPageNumber)
	jreq.Page.Size = jreq.ParamNonNegInt(ParamPageSize)
	jreq.Page.Offset = jreq.ParamNonNegInt(ParamPageOffset)
	jreq.Page.Limit = jreq.ParamNonNegInt(ParamPageLimit)
	jreq.Page.Cursor = jreq.Param(ParamPageCursor)

	return jreq, nil
}

// Param returns a parameter as a string. If the parameter doesn't exist, an
// empty string is returned. Multiple keys can be given to access nested
// parameter values.
func (r Request) Param(ks ...string) string {
	return r.Params.Get(JoinParamKey(ks))
}

// ParamInt is like Param, except it returns an int or 0 if missing or invalid.
func (r Request) ParamInt(ks ...string) int {
	if v, err := strconv.Atoi(r.Param(ks...)); err == nil {
		return v
	}
	return 0
}

// ParamNonNegInt is like ParamInt, except negative integers are invalid.
func (r Request) ParamNonNegInt(ks ...string) int {
	if v := r.ParamInt(ks...); v >= 0 {
		return v
	}
	return 0
}

// Include returns true if the given relationship path p should be included in
// the response. This is true if the request did not specify any includes or
// p was included in the list of includes.
func (r Request) Include(p string) bool {
	if r.Includes == nil {
		return true
	}
	for _, v := range r.Includes {
		if v == p {
			return true
		}
	}
	return false
}

// Filter returns the value of a filter parameter. If a filter doesn't exist, an
// empty string is returned. Keys can be given to access nested filters.
func (r Request) Filter(ks ...string) string {
	ks = append([]string{ParamFilter}, ks...)
	return r.Param(ks...)
}
