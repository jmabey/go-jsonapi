package jsonapi

import "strings"

// Error is a JSON API error object.
type Error struct {
	ID     string `json:"id,omitempty"`
	Status int    `json:"status,string,omitempty"`
	Code   string `json:"code,omitempty"`
	Title  string `json:"title,omitempty"`
	Detail string `json:"detail,omitempty"`
	Source Meta   `json:"source,omitempty"`
	Links  Links  `json:"links,omitempty"`
	Meta   Meta   `json:"meta,omitempty"`
}

// Error returns the error details.
func (e Error) Error() string {
	return e.Detail
}

// Errors is a slice of JSON API error objects.
type Errors []Error

// Error returns all error details as a string.
func (es Errors) Error() string {
	out := []string{}
	for _, e := range es {
		out = append(out, e.Error())
	}
	return strings.Join(out, "; ")
}
