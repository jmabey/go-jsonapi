package jsonapi

import (
	"net/http"

	"golang.org/x/net/context"
)

// ErrorHandler is a context handler that calls a function that can return an
// error that gets added to the JSON API response.
type ErrorHandler func(ctx context.Context, rw http.ResponseWriter, req *http.Request) error

// ServeHTTP calls the function and adds any error returned to the JSON API
// response.
func (eh ErrorHandler) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	jres := ResponseFromContext(ctx)
	if err := eh(ctx, rw, req); err != nil {
		jres.AddError(err)
	}
}
