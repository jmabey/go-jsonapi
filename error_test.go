package jsonapi

import "testing"

func TestError(t *testing.T) {
	err := Error{Detail: "error details"}
	if got, want := err.Error(), err.Detail; got != want {
		t.Errorf("got %q; want %q", got, want)
	}
}

func TestErrors(t *testing.T) {
	errs := Errors{
		{Detail: "error 1"},
		{Detail: "error 2"},
	}
	if got, want := errs.Error(), "error 1; error 2"; got != want {
		t.Errorf("got %q; want %q", got, want)
	}
}
