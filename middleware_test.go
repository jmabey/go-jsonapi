package jsonapi

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

func SetUpParamHeaders(t *testing.T) (context.Context, *MockWriter, *http.Request) {
	ctx, rw, req := SetUpParams(t)
	req.Header.Set("Accept", "application/json, application/vnd.api+json")
	req.Header.Set("Content-Type", ContentType)
	return ctx, rw, req
}

func TestMiddleware(t *testing.T) {
	ctx, rw, req := SetUpParamHeaders(t)
	call := false

	h := web.ContextHandlerFunc(func(hctx context.Context, rw http.ResponseWriter, req *http.Request) {
		if RequestFromContext(hctx) == nil {
			t.Error("missing request in context")
		}
		if ResponseFromContext(hctx) == nil {
			t.Error("missing response in context")
		}
		call = true
	})

	Middleware{Handler: h}.ServeHTTP(ctx, rw, req)

	if !call {
		t.Error("handler not called")
	}
	if len(rw.Body) == 0 {
		t.Error("body not written")
	}
}

func TestMiddlewareErrors(t *testing.T) {
	ctx, rw, req := SetUpParams(t)
	req.Body = ioutil.NopCloser(bytes.NewBufferString("{"))
	h := &StubHandler{}

	Middleware{Handler: h}.ServeHTTP(ctx, rw, req)

	res := &Response{}
	if err := json.Unmarshal(rw.Body, res); err != nil {
		t.Fatalf("unmarshal error: %s", err)
	}
	if h.Called {
		t.Error("unexpected handler call")
	}
	if got, want := len(res.Errors), 2; got != want {
		log.Println(res.Errors)
		t.Fatalf("got %d errors, want %d", got, want)
	}
	if got, want := res.Errors[0].Status, http.StatusBadRequest; got != want {
		t.Errorf("got status %d; want %d", got, want)
	}
	if got, want := res.Errors[0].Detail, "Invalid JSON body: unexpected EOF"; got != want {
		t.Errorf("got detail %q; want %q", got, want)
	}

	ctx, rw, req = SetUpParams(t)
	req.Method = http.MethodPost
	req.Body = ioutil.NopCloser(bytes.NewBufferString(`{"data":{"id":"id","type": "bad"}}`))
	req.URL.RawQuery = "bad=bad&goodParam=good&include=bad&sort=bad"

	Middleware{Type: "type", DataTypes: DataNull, Handler: h}.ServeHTTP(ctx, rw, req)

	if err := json.Unmarshal(rw.Body, res); err != nil {
		t.Fatalf("unmarshal error: %s", err)
	}
	if h.Called {
		t.Error("unexpected handler call")
	}
	if got, want := len(res.Errors), 7; got != want {
		t.Fatalf("got %d errors; want %d", got, want)
	}
	for i, tt := range []struct {
		s int
		d string
	}{
		{
			http.StatusNotAcceptable,
			`Request must have "application/vnd.api+json" type in Accept header.`,
		},
		{
			http.StatusBadRequest,
			`Request has invalid parameter "bad".`,
		},
		{
			http.StatusBadRequest,
			`Request has unsupported include "bad".`,
		},
		{
			http.StatusBadRequest,
			`Request has unsupported sort field "bad".`,
		},
		{
			http.StatusForbidden,
			ErrClientID.Detail,
		},
		{
			http.StatusConflict,
			`Resources must have type "type".`,
		},
		{
			http.StatusBadRequest,
			ErrDataSingle.Detail,
		},
	} {
		if got := res.Errors[i].Status; got != tt.s {
			t.Errorf("error %d: got status %d; want %d", i, got, tt.s)
		}
		if got := res.Errors[i].Detail; got != tt.d {
			t.Errorf("error %d: got detail %q; want %q", i, got, tt.d)
		}
	}
}

func TestMiddlewareFieldsets(t *testing.T) {
	ctx, rw, req := SetUpParamHeaders(t)
	req.URL.RawQuery = "fields[type]=one"

	h := web.ContextHandlerFunc(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
		jres := ResponseFromContext(ctx)

		if got, want := jres.Fieldsets, (Fieldsets{"type": []string{"one"}}); !reflect.DeepEqual(got, want) {
			t.Errorf("got %v; want %v", got, want)
		}
	})
	Middleware{Handler: h}.ServeHTTP(ctx, rw, req)
}

func TestMiddlewareSorts(t *testing.T) {
	ctx, rw, req := SetUpParamHeaders(t)
	req.URL.RawQuery = "sort=one"
	ss := Sorts{{Name: "one", Expr: "expr", ExprAsc: "asc", ExprDesc: "desc"}}
	call := false

	h := web.ContextHandlerFunc(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
		jreq := RequestFromContext(ctx)
		if !reflect.DeepEqual(jreq.Sorts, ss) {
			t.Errorf("got request %#v; want %#v", jreq.Sorts, ss)
		}
		call = true
	})
	Middleware{Sorts: ss, Handler: h}.ServeHTTP(ctx, rw, req)
}

func TestMiddlewarePageMaxes(t *testing.T) {
	for i, tt := range []struct {
		inSize, inLimit   int
		outSize, outLimit int
	}{
		{0, 0, 100, 200},
		{10, 20, 10, 20},
	} {
		ctx, rw, req := SetUpParamHeaders(t)
		req.URL.RawQuery = "page[size]=100&page[limit]=200"

		h := web.ContextHandlerFunc(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
			jreq := RequestFromContext(ctx)

			if got, want := jreq.Page.Size, tt.outSize; got != want {
				t.Errorf("%d: got size %d; want %d", i, got, want)
			}
			if got, want := jreq.Page.Limit, tt.outLimit; got != want {
				t.Errorf("%d: got limit %d; want %d", i, got, want)
			}
		})
		Middleware{
			MaxPageSize:  tt.inSize,
			MaxPageLimit: tt.inLimit,
			Handler:      h,
		}.ServeHTTP(ctx, rw, req)
	}
}

func TestMiddlewareWriteResponse(t *testing.T) {
	for i, tt := range []struct {
		method string
		status int
		want   int
	}{
		{http.MethodGet, 0, http.StatusOK},
		{http.MethodPost, 0, http.StatusCreated},
		{http.MethodPut, 0, http.StatusOK},
		{http.MethodDelete, 0, http.StatusNoContent},
		{http.MethodPost, http.StatusOK, http.StatusOK},
	} {
		ctx, rw, req := SetUpParamHeaders(t)
		req.Method = tt.method

		h := web.ContextHandlerFunc(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
			jres := ResponseFromContext(ctx)
			jres.Status = tt.status
		})
		Middleware{DataTypes: DataNull, Handler: h}.ServeHTTP(ctx, rw, req)

		if rw.Status != tt.want {
			t.Errorf("%d: got status %d; want %d", i, rw.Status, tt.want)
		}
	}
}
