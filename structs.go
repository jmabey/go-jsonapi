package jsonapi

// Meta stores arbitrary, non-standard data.
type Meta map[string]interface{}

// Relationship represents a relationship object and stores a reference to
// a resource object.
type Relationship struct {
	Links Links `json:"links,omitempty"`
	Data  *Data `json:"data,omitempty"`
	Meta  Meta  `json:"meta,omitempty"`
}

// Relationships is a map of relationship objects.
type Relationships map[string]*Relationship

// Version is the version of JSON API that this package implements.
const Version = "1.0"

// JSONAPI stores details about this implementation of JSON API.
type JSONAPI struct {
	Version string `json:"version,omitempty"`
	Meta    Meta   `json:"meta,omitempty"`
}
