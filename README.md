jsonapi
=======

[![GoDoc](https://godoc.org/gitlab.com/jmabey/go-jsonapi?status.svg)](https://godoc.org/gitlab.com/jmabey/go-jsonapi) [![build status](https://gitlab.com/jmabey/go-jsonapi/badges/master/build.svg)](https://gitlab.com/jmabey/go-jsonapi/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/jmabey/go-jsonapi)](https://goreportcard.com/report/gitlab.com/jmabey/go-jsonapi)
