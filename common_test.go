package jsonapi

import (
	"net/http"
	"testing"

	"golang.org/x/net/context"
)

type MockWriter struct {
	h      http.Header
	Body   []byte
	Status int
}

func NewMockWriter() *MockWriter {
	return &MockWriter{h: http.Header{}}
}

func (m MockWriter) Header() http.Header {
	return m.h
}

func (m *MockWriter) Write(b []byte) (int, error) {
	m.Body = b
	return 0, nil
}

func (m *MockWriter) WriteHeader(s int) {
	m.Status = s
}

type StubHandler struct {
	Called bool
}

func (h *StubHandler) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	h.Called = true
}

func SetUpParams(t *testing.T) (context.Context, *MockWriter, *http.Request) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatalf("unexpected request error %q", err)
	}
	return context.Background(), NewMockWriter(), req
}
