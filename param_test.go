package jsonapi

import (
	"reflect"
	"testing"
)

func TestSplitComma(t *testing.T) {
	for i, tt := range []struct {
		in  string
		out []string
	}{
		{"", []string{}},
		{"a", []string{"a"}},
		{"a,b", []string{"a", "b"}},
		{"a,b,c", []string{"a", "b", "c"}},
		{"a,,,b", []string{"a", "", "", "b"}},
	} {
		if got := SplitComma(tt.in); !reflect.DeepEqual(got, tt.out) {
			t.Errorf("%d: got %v; want %v", i, got, tt.out)
		}
	}
}

func TestSplitParamKey(t *testing.T) {
	for i, tt := range []struct {
		in  string
		out []string
	}{
		{"", []string{""}},
		{"key", []string{"key"}},
		{"key[one]", []string{"key", "one"}},
		{"key[one][two]", []string{"key", "one", "two"}},
		{"key[one][two][three]", []string{"key", "one", "two", "three"}},
		{"key[one", []string{"key[one"}},
		{"key[one[two]", []string{"key", "one[two"}},
	} {
		if got := SplitParamKey(tt.in); !reflect.DeepEqual(got, tt.out) {
			t.Errorf("%d: got %v; want %v", i, got, tt.out)
		}
	}
}

func TestJoinParamKey(t *testing.T) {
	for i, tt := range []struct {
		in  []string
		out string
	}{
		{[]string{}, ""},
		{[]string{"key"}, "key"},
		{[]string{"key", "one"}, "key[one]"},
		{[]string{"key", "one", "two"}, "key[one][two]"},
		{[]string{"key[one"}, "key[one"},
	} {
		if got := JoinParamKey(tt.in); !reflect.DeepEqual(got, tt.out) {
			t.Errorf("%d: got %v; want %v", i, got, tt.out)
		}
	}
}
