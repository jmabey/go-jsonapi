package jsonapi

import (
	"fmt"
	"net/http"
	"strings"
)

// ContentType is the Content-Type header value used for JSON API requests and
// responses.
const ContentType = "application/vnd.api+json"

// Requirement represents the possible requirements for client-generated IDs.
type Requirement int

// Possible values for Requirement.
const (
	Forbidden Requirement = iota
	Optional
	Required
)

// DataTypeBits specifies the allowed types for the data field of a request.
type DataTypeBits int

// Possible bit flags for request body data types.
const (
	DataNull DataTypeBits = 1 << iota
	DataSingle
	DataArray
)

// Validation errors.
var (
	ErrClientID = Error{
		Status: http.StatusForbidden,
		Detail: "Resources cannot have client-generated IDs.",
	}
	ErrMissingClientID = Error{
		Status: http.StatusForbidden,
		Detail: "Resources must have client-generated IDs.",
	}
	ErrDataNull = Error{
		Status: http.StatusBadRequest,
		Detail: "Request data cannot be null.",
	}
	ErrDataSingle = Error{
		Status: http.StatusBadRequest,
		Detail: "Request data cannot be a single resource.",
	}
	ErrDataArray = Error{
		Status: http.StatusBadRequest,
		Detail: "Request data cannot be an array of resources.",
	}
)

// Validation error format strings.
const (
	ErrAccept  = "Request must have %q type in Accept header."
	ErrParam   = "Request has invalid parameter %q."
	ErrInclude = "Request has unsupported include %q."
	ErrSort    = "Request has unsupported sort field %q."
	ErrType    = "Resources must have type %q."
)

// ValidAccept returns an error if the request's Accept header does not contain
// the JSON API content type.
func ValidAccept(req *http.Request) error {
	for _, v := range SplitComma(req.Header.Get("Accept")) {
		if strings.Trim(v, " ") == ContentType {
			return nil
		}
	}
	return Error{
		Status: http.StatusNotAcceptable,
		Detail: fmt.Sprintf(ErrAccept, ContentType),
	}
}

// ValidParams returns an error if the request contains any invalid query
// parameters.
func ValidParams(req *http.Request) error {
	errs := Errors{}
	for k := range req.URL.Query() {
		ks := SplitParamKey(k)
		if !StandardParams[ks[0]] && RegexpStandardParam.MatchString(ks[0]) {
			errs = append(errs, Error{
				Status: http.StatusBadRequest,
				Detail: fmt.Sprintf(ErrParam, k),
			})
		}
	}
	if len(errs) > 0 {
		return errs
	}
	return nil
}

// ValidIncludes returns an error if the request contains any include paths not
// found in is.
func ValidIncludes(jreq *Request, is Includes) error {
	errs := Errors{}
	for _, p := range jreq.Includes {
		if !is.Contains(p) {
			errs = append(errs, Error{
				Status: http.StatusBadRequest,
				Detail: fmt.Sprintf(ErrInclude, p),
			})
		}
	}
	if len(errs) > 0 {
		return errs
	}
	return nil
}

// ValidSorts returns an error if the request contains any sort field not found
// in ss.
func ValidSorts(jreq *Request, ss Sorts) error {
	errs := Errors{}
	for _, s := range jreq.Sorts {
		if found := ss.Search(s.Name); found == nil {
			errs = append(errs, Error{
				Status: http.StatusBadRequest,
				Detail: fmt.Sprintf(ErrSort, s.Name),
			})
		}
	}
	if len(errs) > 0 {
		return errs
	}
	return nil
}

// ValidClientIDs returns an error if the request contains any client-generated
// IDs that do not meet the given requirement. If the request is not a POST
// request, then no error will be returned.
func ValidClientIDs(req *http.Request, jreq *Request, require Requirement) error {
	if req.Method != http.MethodPost || jreq.Data == nil || require == Optional {
		return nil
	}

	rs := append(Resources{}, jreq.Data.Resources...)
	if jreq.Data.Resource != nil {
		rs = append(rs, jreq.Data.Resource)
	}
	if len(rs) <= 0 {
		return nil
	}

	cid := false
	for _, r := range rs {
		if r.ID != "" {
			cid = true
			break
		}
	}

	if require == Forbidden && cid {
		return ErrClientID
	}
	if require == Required && !cid {
		return ErrMissingClientID
	}

	return nil
}

// ValidType returns an error if the request contains any resources with type
// not equal to t. If t is an empty string, then no error will be returned.
func ValidType(jreq *Request, t string) error {
	if jreq.Data == nil || t == "" {
		return nil
	}

	rs := append(Resources{}, jreq.Data.Resources...)
	if jreq.Data.Resource != nil {
		rs = append(rs, jreq.Data.Resource)
	}

	for _, r := range rs {
		if r.Type != t {
			return Error{
				Status: http.StatusConflict,
				Detail: fmt.Sprintf(ErrType, t),
			}
		}
	}

	return nil
}

// ValidDataTypes returns an error if the request data is a type (null, single
// resource, or array) that is not included in b. If b is zero, then the default
// depends on the request method. POST and PATCH requests only allow a single
// resource and all other methods only allow null.
func ValidDataTypes(req *http.Request, jreq *Request, b DataTypeBits) error {
	if b == 0 {
		switch req.Method {
		case http.MethodPost, http.MethodPatch:
			b = DataSingle
		default:
			b = DataNull
		}
	}

	if b&DataNull == 0 && jreq.Data == nil {
		return ErrDataNull
	}
	if b&DataSingle == 0 && jreq.Data != nil && jreq.Data.Resource != nil {
		return ErrDataSingle
	}
	if b&DataArray == 0 && jreq.Data != nil && jreq.Data.Resources != nil {
		return ErrDataArray
	}

	return nil
}
