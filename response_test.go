package jsonapi

import (
	"bytes"
	"errors"
	"net/http"
	"reflect"
	"testing"
)

func TestHeader(t *testing.T) {
	rw := NewMockWriter()
	rw.Header().Set("Test", "value")
	res := NewResponse(rw)

	if got, want := res.Header(), rw.Header(); !reflect.DeepEqual(got, want) {
		t.Errorf("got %v; want %v", got, want)
	}
}

func TestSetLocation(t *testing.T) {
	rw := NewMockWriter()
	res := NewResponse(rw)

	res.SetLocation("url")

	if got := rw.Header().Get("Location"); got != "url" {
		t.Errorf("got %s; want url", got)
	}
}

func TestSetResource(t *testing.T) {
	res := NewResponse(nil)
	r := &Resource{}

	res.SetResource(r)

	if res.Data.Resource != r {
		t.Error("resource not set")
	}
}

func TestAddResource(t *testing.T) {
	res := NewResponse(nil)
	r := &Resource{}

	res.AddResource(r)

	if got, want := res.Data.Resources, (Resources{r}); !reflect.DeepEqual(got, want) {
		t.Errorf("got %v; want %v", got, want)
	}
}

func TestAddResources(t *testing.T) {
	res := NewResponse(nil)
	rs := Resources{&Resource{}, &Resource{}}

	res.AddResources(Resources{})

	if res.Data == nil || res.Data.Resources == nil {
		t.Error("resources slice was nil")
	}

	res.AddResources(rs)
	res.AddResources(rs)

	if got, want := res.Data.Resources, (Resources{rs[0], rs[1], rs[0], rs[1]}); !reflect.DeepEqual(got, want) {
		t.Errorf("got %v; want %v", got, want)
	}
}

func TestResponseAddInclude(t *testing.T) {
	res := NewResponse(nil)
	r := &Resource{ID: "id", Type: "model"}

	res.AddInclude(r)
	res.AddInclude(r)

	if got, want := len(res.Included), 1; got != want {
		t.Fatalf("got %d resources; want %d", got, want)
	}
	if got := res.Included[0]; got != r {
		t.Errorf("got %v; want %v", got, r)
	}
}

func TestResponseAddIncludes(t *testing.T) {
	res := NewResponse(nil)
	rs := Resources{
		{ID: "id1", Type: "model"},
		{ID: "id1", Type: "model"},
		{ID: "id2", Type: "model"},
	}

	res.AddIncludes(rs)

	if got, want := len(res.Included), 2; got != want {
		t.Errorf("got %d resources; want %d", got, want)
	}
}

func TestResponseAddError(t *testing.T) {
	res := NewResponse(nil)
	err := Error{ID: "error", Status: http.StatusBadRequest}

	res.AddError(err)

	if len(res.Errors) != 1 {
		t.Fatalf("errors is empty")
	}
	if !reflect.DeepEqual(res.Errors[0], err) {
		t.Errorf("got error %v; want %v", res.Errors[0], err)
	}
	if res.Status != err.Status {
		t.Errorf("got status %v; want %v", res.Status, err.Status)
	}
}

func TestResponseAddErrorStandard(t *testing.T) {
	res := NewResponse(nil)
	res.AddError(errors.New("standard"))

	if got, want := res.Errors[0].Detail, "standard"; got != want {
		t.Errorf("got detail %s; want %s", got, want)
	}
	if want := http.StatusInternalServerError; res.Status != want {
		t.Errorf("got status %d; want %d", res.Status, want)
	}
}

func TestResponseAddErrorErrors(t *testing.T) {
	res := NewResponse(nil)
	res.AddError(Errors{
		{Detail: "error 1"},
		{Detail: "error 2"},
	})

	if got, want := len(res.Errors), 2; got != want {
		t.Errorf("got %d error(s); want %d", got, want)
	}
}

func TestResponseAddErrorWithoutStatus(t *testing.T) {
	res := NewResponse(nil)
	res.AddError(Error{})

	if want := http.StatusInternalServerError; res.Status != want {
		t.Errorf("got status %v; want %v", res.Status, want)
	}
}

func TestResponseAddErrorMultiple(t *testing.T) {
	res := NewResponse(nil)

	res.AddError(Error{})
	res.AddError(Error{})
	if want := http.StatusInternalServerError; res.Status != want {
		t.Errorf("got %v after 2 errors; want %v", res.Status, want)
	}

	res.AddError(Error{Status: http.StatusUnauthorized})
	if want := http.StatusUnauthorized; res.Status != want {
		t.Errorf("got %v after 3 errors; want %v", res.Status, want)
	}

	res.AddError(Error{Status: http.StatusUnauthorized})
	if want := http.StatusUnauthorized; res.Status != want {
		t.Errorf("got %v after 4 errors; want %v", res.Status, want)
	}

	res.AddError(Error{Status: http.StatusForbidden})
	if want := http.StatusBadRequest; res.Status != want {
		t.Errorf("got %v after 5 errors; want %v", res.Status, want)
	}

	res.AddError(Error{Status: http.StatusBadGateway})
	if want := http.StatusInternalServerError; res.Status != want {
		t.Errorf("got %v after 6 errors; want %v", res.Status, want)
	}
}

func TestWrite(t *testing.T) {
	rw := NewMockWriter()
	res := NewResponse(rw)

	res.Write()

	if got := rw.Header().Get("Content-Type"); got != ContentType {
		t.Errorf("got content type %q; want %q", got, ContentType)
	}
	if rw.Status != http.StatusOK {
		t.Errorf("got status %v; want %v", rw.Status, http.StatusOK)
	}
	if len(rw.Body) == 0 {
		t.Errorf("body not written")
	}

	rw = NewMockWriter()
	res = NewResponse(rw)
	res.Status = http.StatusNoContent

	res.Write()

	if len(rw.Body) != 0 {
		t.Error("unexpected body written")
	}
}

func TestWriteFieldsets(t *testing.T) {
	fs := Fieldsets{"type": []string{"one"}}
	as := Attributes{"one": 1, "two": 2}

	for i, tt := range []struct {
		d   *Data
		inc Resources
	}{
		{
			&Data{Resource: &Resource{Type: "type", Attributes: as}},
			nil,
		},
		{
			&Data{Resources: Resources{{Type: "type", Attributes: as}}},
			nil,
		},
		{
			nil,
			Resources{{Type: "type", Attributes: as}},
		},
	} {
		rw := NewMockWriter()
		res := NewResponse(rw)
		res.Fieldsets = fs
		res.Data = tt.d
		res.Included = tt.inc
		res.Write()

		if bytes.Contains(rw.Body, []byte(`"two"`)) {
			t.Errorf("%d: fieldsets not applied", i)
		}
	}
}
