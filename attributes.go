package jsonapi

import (
	"time"

	"gopkg.in/guregu/null.v3"
)

// Attributes represents an attributes object and stores arbitrary data.
type Attributes map[string]interface{}

// NullBool returns the value of a nullable bool attribute. It will return null
// for non-bool or missing attributes.
func (a Attributes) NullBool(k string) null.Bool {
	if v, ok := a[k].(bool); ok {
		return null.BoolFrom(v)
	}
	return null.Bool{}
}

// Bool returns the value of a bool attribute. It will return false for non-bool
// or missing attributes.
func (a Attributes) Bool(k string) bool {
	return a.NullBool(k).Bool
}

// NullFloat returns the value of a nullable float64 attribute. It will return
// null for non-float64 or missing attributes.
func (a Attributes) NullFloat(k string) null.Float {
	if v, ok := a[k].(float64); ok {
		return null.FloatFrom(v)
	}
	return null.Float{}
}

// Float returns the value of a float64 attribute. It will return 0 for
// non-float64 or missing attributes.
func (a Attributes) Float(k string) float64 {
	return a.NullFloat(k).Float64
}

// NullInt returns the value of a nullable int attribute. It will return null
// for non-int or missing attributes.
func (a Attributes) NullInt(k string) null.Int {
	switch v := a[k].(type) {
	// JSON numbers are stored as float64 in interface{} values.
	case float64:
		return null.IntFrom(int64(v))
	case int:
		return null.IntFrom(int64(v))
	case int64:
		return null.IntFrom(v)
	}
	return null.Int{}
}

// Int returns the value of an int attribute. It will return null for non-int or
// missing attributes.
func (a Attributes) Int(k string) int {
	return int(a.NullInt(k).Int64)
}

// NullString returns the value of a nullable string attribute. It will return
// null for non-string or missing attributes.
func (a Attributes) NullString(k string) null.String {
	if s, ok := a[k].(string); ok {
		return null.StringFrom(s)
	}
	return null.String{}
}

// String returns the value of a string attribute or "" for non-string or
// missing attributes.
func (a Attributes) String(k string) string {
	return a.NullString(k).String
}

// NullTime returns the value of a nullable time attribute. It will return null
// for non-time or missing attributes.
func (a Attributes) NullTime(k string) null.Time {
	if s, ok := a[k].(string); ok {
		if t, err := time.Parse(time.RFC3339, s); err == nil {
			return null.TimeFrom(t)
		}
	}
	return null.Time{}
}

// Time returns the value of a time attribute. It will return the zero time
// for non-time or missing attributes.
func (a Attributes) Time(k string) time.Time {
	return a.NullTime(k).Time
}
