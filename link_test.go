package jsonapi

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestLinkMarshal(t *testing.T) {
	for i, tt := range []struct {
		in  Link
		out []byte
	}{
		{
			Link{Href: "http://example.com"},
			[]byte(`"http://example.com"`),
		},
		{
			Link{Href: "http://example.com", Meta: Meta{"value": true}},
			[]byte(`{"href":"http://example.com","meta":{"value":true}}`),
		},
	} {
		b, err := json.Marshal(tt.in)
		if err != nil {
			t.Errorf("%d: unexpected error %q", i, err)
		}
		if !reflect.DeepEqual(b, tt.out) {
			t.Errorf("%d: got %s; want %s", i, b, tt.out)
		}
	}
}
