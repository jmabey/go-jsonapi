package jsonapi

import "testing"

func TestSortString(t *testing.T) {
	for i, tt := range []struct {
		in        Sort
		asc, desc string
	}{
		{
			Sort{Name: "test"},
			"test ASC",
			"test DESC",
		},
		{
			Sort{Name: "test", Expr: "lower(test)"},
			"lower(test) ASC",
			"lower(test) DESC",
		},
		{
			Sort{Name: "test", ExprAsc: "lower(test) ASC"},
			"lower(test) ASC",
			"test DESC",
		},
		{
			Sort{Name: "test", ExprDesc: "lower(test) DESC"},
			"test ASC",
			"lower(test) DESC",
		},
		{
			Sort{Name: "test", ExprAsc: "lower(test) ASC", ExprDesc: "lower(test) DESC"},
			"lower(test) ASC",
			"lower(test) DESC",
		},
	} {
		if got := tt.in.String(); got != tt.asc {
			t.Errorf("%d: got asc %q; want %q", i, got, tt.asc)
		}

		tt.in.Desc = true
		if got := tt.in.String(); got != tt.desc {
			t.Errorf("%d: got desc %q; want %q", i, got, tt.desc)
		}
	}
}

func TestSortsString(t *testing.T) {
	ss := Sorts{
		{Name: "one"},
		{Name: "two", Desc: true},
	}

	if got, want := ss.String(), "one ASC, two DESC"; got != want {
		t.Errorf("got %s; want %s", got, want)
	}
}

func TestSortsSearch(t *testing.T) {
	ss := Sorts{{Name: "one"}, {Name: "two"}}

	if got := ss.Search("two"); got != ss[1] {
		t.Errorf("got sort %#v; want %#v", got, ss[1])
	}
	if got := ss.Search("bad"); got != nil {
		t.Errorf("unexpected find: %#v", got)
	}
}
