package jsonapi

import "encoding/json"

// linkFields are the fields of a link. This needs to be its own type to avoid
// an infinite loop when marshalling a link object.
type linkFields struct {
	Href string `json:"href,omitempty"`
	Meta Meta   `json:"meta,omitempty"`
}

// Link represents a hyperlink.
type Link linkFields

// Links is a map containing multiple links.
type Links map[string]Link

// MarshalJSON marshals the link to either a string (if it has no meta data) or
// an object.
func (l Link) MarshalJSON() ([]byte, error) {
	if len(l.Meta) == 0 {
		return json.Marshal(l.Href)
	}
	return json.Marshal(linkFields{l.Href, l.Meta})
}
