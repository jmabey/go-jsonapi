package jsonapi

import (
	"fmt"
	"strings"
)

// SQL expression constants for ascending and descending order.
const (
	Asc  = "ASC"
	Desc = "DESC"
)

// Sort defines a sort field.
type Sort struct {
	// Name is the name of the sort field that is used in the sort parameter.
	Name string

	// Expr is the actual expression (or column name) that is prefixed with ASC
	// or DESC depending on the order for this sort field.
	Expr string

	// ExprAsc and ExprDesc are the actual expressions for ascending and
	// descending order for this sort field.
	ExprAsc, ExprDesc string

	// Desc is true if this field should be sorted in descending order.
	Desc bool
}

// String converts a sort field to a string. This prefers using ExprAsc and
// ExprDesc, then Expr with ASC or DESC suffix, then Name with ASC or DESC
// suffix.
func (s Sort) String() string {
	dir := Asc
	out := s.ExprAsc
	if s.Desc {
		dir = Desc
		out = s.ExprDesc
	}

	if out == "" {
		out = s.Expr
		if out == "" {
			out = s.Name
		}
		out = fmt.Sprintf("%s %s", out, dir)
	}

	return out
}

// Sorts is a slice of sort fields.
type Sorts []*Sort

// String converts the slice into a comma-separated list as a string.
func (ss Sorts) String() string {
	out := []string{}
	for _, s := range ss {
		out = append(out, s.String())
	}
	return strings.Join(out, ", ")
}

// Search returns a sort field by name or nil if not found.
func (ss Sorts) Search(n string) *Sort {
	for _, s := range ss {
		if s.Name == n {
			return s
		}
	}
	return nil
}
