package jsonapi

import (
	"testing"

	"golang.org/x/net/context"
)

func TestRequestContext(t *testing.T) {
	ctx := context.Background()
	req := &Request{}

	ctx = NewRequestContext(ctx, req)
	if got := RequestFromContext(ctx); got != req {
		t.Errorf("got %v; want %v", got, req)
	}
}

func TestResponseContext(t *testing.T) {
	ctx := context.Background()
	res := NewResponse(nil)

	ctx = NewResponseContext(ctx, res)
	if got := ResponseFromContext(ctx); got != res {
		t.Errorf("got %v; want %v", got, res)
	}
}
