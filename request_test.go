package jsonapi

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"
)

func SetUpRequest(t *testing.T, url string) *Request {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("unexpected HTTP request error: %s", err)
	}
	jreq, err := NewRequest(req)
	if err != nil {
		t.Fatalf("unexpected request error: %s", err)
	}
	return jreq
}

func TestRequestData(t *testing.T) {
	for i, tt := range []struct {
		body io.ReadCloser
		err  string
		data *Data
	}{
		{
			nil,
			"",
			nil,
		},
		{
			ioutil.NopCloser(bytes.NewBufferString(``)),
			"",
			nil,
		},
		{
			ioutil.NopCloser(bytes.NewBufferString(`{`)),
			"Invalid JSON body: unexpected EOF",
			nil,
		},
		{
			ioutil.NopCloser(bytes.NewBufferString(`{"data":{}}`)),
			"",
			&Data{Resource: &Resource{}},
		},
	} {
		req, err := http.NewRequest("GET", "/", tt.body)
		if err != nil {
			t.Errorf("%d: unexpected request error: %s", i, err)
			continue
		}

		jreq, err := NewRequest(req)
		if tt.err == "" && err != nil {
			t.Errorf("%d: unexpected error: %s", i, err)
		}
		if tt.err != "" && (err == nil || err.Error() != tt.err) {
			t.Errorf("%d: got error %q; want %q", i, err, tt.err)
		}
		if !reflect.DeepEqual(jreq.Data, tt.data) {
			t.Errorf("%d: got data %v; want %v", i, jreq.Data, tt.data)
		}
	}
}

func TestRequestParam(t *testing.T) {
	req := SetUpRequest(t, "/?key=one&key[one]=two&key[one][two]=three")
	for i, tt := range []struct {
		in  []string
		out string
	}{
		{[]string{}, ""},
		{[]string{"bad"}, ""},
		{[]string{"key"}, "one"},
		{[]string{"key", "one"}, "two"},
		{[]string{"key", "one", "two"}, "three"},
		{[]string{"key[one][two]"}, "three"},
	} {
		if got := req.Param(tt.in...); got != tt.out {
			t.Errorf("%d: got %s; want %s", i, got, tt.out)
		}
	}
}

func TestRequestParamInt(t *testing.T) {
	req := SetUpRequest(t, "/?n=-123&z=0&p=123&f=123.456&bad=bad")
	for i, tt := range []struct {
		in          string
		out, nonNeg int
	}{
		{"n", -123, 0},
		{"z", 0, 0},
		{"p", 123, 123},
		{"f", 0, 0},
		{"bad", 0, 0},
		{"missing", 0, 0},
	} {
		if got := req.ParamInt(tt.in); got != tt.out {
			t.Errorf("%d: got int %d; want %d", i, got, tt.out)
		}
		if got := req.ParamNonNegInt(tt.in); got != tt.nonNeg {
			t.Errorf("%d: got non-negative int %d; want %d", i, got, tt.nonNeg)
		}
	}
}

func TestRequestInclude(t *testing.T) {
	for i, tt := range []struct {
		url, in string
		out     bool
	}{
		{"", "any", true},
		{"?include=key", "any", false},
		{"?include=key", "key", true},
		{"?include=one,two", "two", true},
	} {
		req := SetUpRequest(t, tt.url)
		if ok := req.Include(tt.in); ok != tt.out {
			t.Errorf("%d: got %t; want %t", i, ok, tt.out)
		}
	}
}

func TestRequestFieldsets(t *testing.T) {
	req := SetUpRequest(t, "/?fields[type]=a,b,c&fields[one][two]=d")
	want := Fieldsets{
		"type":     []string{"a", "b", "c"},
		"one][two": []string{"d"},
	}
	if !reflect.DeepEqual(req.Fieldsets, want) {
		t.Errorf("got %v; want %v", req.Fieldsets, want)
	}
}

func TestRequestSorts(t *testing.T) {
	req := SetUpRequest(t, "/?sort=one,-two")
	want := Sorts{
		{Name: "one"},
		{Name: "two", Desc: true},
	}
	if !reflect.DeepEqual(req.Sorts, want) {
		t.Errorf("got %q; want %q", req.Sorts, want)
	}
}

func TestRequestPage(t *testing.T) {
	req := SetUpRequest(t, "/?page[number]=1&page[size]=2&page[offset]=3&page[limit]=4&page[cursor]=cursor")
	if got, want := req.Page.Number, 1; got != want {
		t.Errorf("got number %d; want %d", got, want)
	}
	if got, want := req.Page.Size, 2; got != want {
		t.Errorf("got size %d; want %d", got, want)
	}
	if got, want := req.Page.Offset, 3; got != want {
		t.Errorf("got offset %d; want %d", got, want)
	}
	if got, want := req.Page.Limit, 4; got != want {
		t.Errorf("got limit %d; want %d", got, want)
	}
	if got, want := req.Page.Cursor, "cursor"; got != want {
		t.Errorf("got cursor %s; want %s", got, want)
	}
}

func TestRequestFilter(t *testing.T) {
	req := SetUpRequest(t, "?filter=one&filter[one]=two&filter[one][two]=three")
	for i, tt := range []struct {
		in  []string
		out string
	}{
		{[]string{}, "one"},
		{[]string{"bad"}, ""},
		{[]string{"one"}, "two"},
		{[]string{"one", "two"}, "three"},
	} {
		if got := req.Filter(tt.in...); got != tt.out {
			t.Errorf("%d: got %s; want %s", i, got, tt.out)
		}
	}
}
