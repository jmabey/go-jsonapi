package jsonapi

import (
	"net/http"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

// Middleware is a middleware context handler that performs some JSON API
// validation on the current request, adds JSON API request and response objects
// to the current context for the next handler, and writes the response.
type Middleware struct {
	// Includes is a list of relationship paths supported by the include
	// parameter. Use Any to accept all paths.
	Includes Includes

	// Sorts defines the supported sort fields. The expression fields defined
	// here will be copied into the request's sort fields so the expressions
	// will be available to the handler.
	Sorts Sorts

	// The maximum allowed page size and limit. If 0, then no limit is enforced.
	MaxPageSize, MaxPageLimit int

	// ClientIDs configures the validation check for client-generated IDs in the
	// request body. Possible values are Forbidden (the default), Optional, or
	// Required. Only applies to POST requests.
	ClientIDs Requirement

	// Type is the resource type that all resources in the request must have. If
	// this is an empty string, then all types are allowed.
	Type string

	// DataTypes is a bit field that represents the types that are allowed for
	// the request body data. Possible values are DataNull, DataSingle, and/or
	// DataArray. Use OR (`|`) to allow multiple types. The defaults are
	// DataSingle for POST and PATCH requests and DataNull for other methods.
	DataTypes DataTypeBits

	// Handler is the context handler that is called if the request is valid.
	Handler web.ContextHandler
}

// ServeHTTP performs validation, initializes the request and response objects,
// and calls the next handler.
func (mw Middleware) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	jres := NewResponse(rw)
	defer jres.Write()

	jreq, err := NewRequest(req)
	if err != nil {
		jres.AddError(err)
	}
	if err := ValidAccept(req); err != nil {
		jres.AddError(err)
	}
	if err := ValidParams(req); err != nil {
		jres.AddError(err)
	}
	if err := ValidIncludes(jreq, mw.Includes); err != nil {
		jres.AddError(err)
	}
	if err := ValidSorts(jreq, mw.Sorts); err != nil {
		jres.AddError(err)
	}
	if err := ValidClientIDs(req, jreq, mw.ClientIDs); err != nil {
		jres.AddError(err)
	}
	if err := ValidType(jreq, mw.Type); err != nil {
		jres.AddError(err)
	}
	if err := ValidDataTypes(req, jreq, mw.DataTypes); err != nil {
		jres.AddError(err)
	}
	if len(jres.Errors) > 0 {
		return
	}

	jres.Fieldsets = jreq.Fieldsets
	for _, rs := range jreq.Sorts {
		s := mw.Sorts.Search(rs.Name)
		rs.Expr = s.Expr
		rs.ExprAsc = s.ExprAsc
		rs.ExprDesc = s.ExprDesc
	}
	if mw.MaxPageSize > 0 && jreq.Page.Size > mw.MaxPageSize {
		jreq.Page.Size = mw.MaxPageSize
	}
	if mw.MaxPageLimit > 0 && jreq.Page.Limit > mw.MaxPageLimit {
		jreq.Page.Limit = mw.MaxPageLimit
	}

	hctx := NewRequestContext(ctx, jreq)
	hctx = NewResponseContext(hctx, jres)
	mw.Handler.ServeHTTP(hctx, rw, req)

	if jres.Status == 0 {
		switch req.Method {
		case http.MethodPost:
			jres.Status = http.StatusCreated
		case http.MethodDelete:
			jres.Status = http.StatusNoContent
		}
	}
}
