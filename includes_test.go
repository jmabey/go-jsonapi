package jsonapi

import "testing"

func TestIncludesContains(t *testing.T) {
	for i, tt := range []struct {
		incs Includes
		in   string
		out  bool
	}{
		{Includes{}, "x", false},
		{Includes{"x", "y", "z"}, "x", true},
		{Includes{"x", "y", "z"}, "z", true},
		{Includes{"x", "y", "z"}, "a", false},
		{Includes{"x", Wildcard}, "a", true},
	} {
		if got := tt.incs.Contains(tt.in); got != tt.out {
			t.Errorf("%d: got %t; want %t", i, got, tt.out)
		}
	}
}
