package jsonapi

import (
	"errors"
	"net/http"
	"testing"

	"golang.org/x/net/context"
)

func TestErrorHandler(t *testing.T) {
	res := NewResponse(nil)
	ctx, rw, req := SetUpParams(t)
	ctx = NewResponseContext(ctx, res)

	ErrorHandler(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) error {
		return nil
	}).ServeHTTP(ctx, rw, req)
	if got, want := len(res.Errors), 0; got != want {
		t.Errorf("got %d error(s); want %d", got, want)
	}

	ErrorHandler(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) error {
		return errors.New("error")
	}).ServeHTTP(ctx, rw, req)
	if got, want := len(res.Errors), 1; got != want {
		t.Errorf("got %d error(s); want %d", got, want)
	}
}
